'''
Project 2: 	Complex Searching (The Traveling Salesman)
Programmer: Brandon Campbell (Framework by Dr. Brian Bennett)
Date: 		2/25/2018
Purpose:	Attempt to solve the Traveling Salesman problem using a Genetic Algorithm.
			This particular program uses Elitism for Selection,
			Ordered Crossover for Reproduction,
			and Reverse Sequence Mutation (or a small chance of swapping of consecutive 'nodes') for Mutation
'''

import matplotlib.pyplot as plt
import numpy as np
import random
import copy
import math
import operator
import itertools as intertool

plt.ion()
plt.figure(figsize=(10,5))

def plotTSP(generation, path, points, path_distance, save, num_iters=1):

	"""
	generation: The generation number to display
	path: List of lists with the different orders in which the nodes are visited
	points: coordinates for the different nodes
	path_distance: the distance to display in the figure
	save: True if saving to final_route.png, False otherwise
	num_iters: number of paths that are in the path list

	SOURCE: https://gist.github.com/payoung/6087046

	"""
	### MOD: Brian Bennett

	plt.suptitle("Tennessee Traveling Salesman - Generation " + str(generation) + \
				 "\nPath Length: " + str(path_distance))
	### END MOD

	# Unpack the primary TSP path and transform it into a list of ordered
	# coordinates

	x = []; y = []
	for i in path:
		x.append(points[i][0])
		y.append(points[i][1])

	plt.plot(x, y, 'ko')

	# Set a scale for the arrow heads (there should be a reasonable default for this, WTF?)
	a_scale = 2.5
	# Draw the older paths, if provided
	if num_iters > 1:

		for i in range(1, num_iters):

			# Transform the old paths into a list of coordinates
			xi = []; yi = [];
			for j in paths[i]:
				xi.append(points[j][0])
				yi.append(points[j][1])

			plt.arrow(xi[-1], yi[-1], (xi[0] - xi[-1]), (yi[0] - yi[-1]),
					head_width = a_scale, color = 'r',
					length_includes_head = True, ls = 'dashed',
					width = 0.001/float(num_iters))
			for i in range(0, len(x) - 1):
				plt.arrow(xi[i], yi[i], (xi[i+1] - xi[i]), (yi[i+1] - yi[i]),
						head_width = a_scale, color = 'r', length_includes_head = True,
						ls = 'dashed', width = 0.001/float(num_iters))

	# Draw the primary path for the TSP problem
	plt.arrow(x[-1], y[-1], (x[0] - x[-1]), (y[0] - y[-1]), head_width = a_scale,
			color ='b', length_includes_head=True)
	for i in range(0,len(x)-1):
		plt.arrow(x[i], y[i], (x[i+1] - x[i]), (y[i+1] - y[i]), head_width = a_scale,
				color = 'b', length_includes_head = True)

	if save:
		plt.savefig("final_route.png")

	plt.pause(1)

class GeneticSearch:
	"""
		Class: GeneticSearch
	"""
	def __init__(self, origin, generations, points, cities, population_size, mutation_rate):
		self.population = None
		self.points = points
		self.cities = cities
		self.chromosome_size = len(self.points)
		self.generations = generations
		self.population_size = population_size
		self.mutation_rate = mutation_rate
		self.origin = origin
		self.origin_index = self.points.index(self.origin)
		self.values = []


	def print_population(self, generation, chromosomes):
		index = 0
		print("===== GENERATION %d" % generation)
		for chromosome in self.population:
			print ("Index %5d , Fitness %0.4f : %s" % (index,chromosome[1], ''.join(str(chromosome[0]))))
			index = index + 1
			if index > chromosomes:
				break


	def initialize_population(self):

		self.population = []

		#       You may adjust this code in any way that you believe would help.
		for i in range(self.population_size):

			individual = [x for x in range(self.chromosome_size)] 	# fill the individual with integers 0 - 49 ordered.
			random.shuffle(individual)								# randomize the integers.

			# Move the origin_index to the front of the path
			individual.remove(self.origin_index)
			individual = [self.origin_index] + individual

			fitness = self.fitnessfcn(individual)

			# Prevent duplicate individuals in the initial population
			while [individual,fitness] in self.population:
				individual = [x for x in range(self.chromosome_size)]
				random.shuffle(individual)

				individual.remove(self.origin_index)
				individual = [self.origin_index] + individual

				fitness = self.fitnessfcn(individual)

			# POPULATION NODES are in the form [chromosome, fitness]
			self.population.append([individual,fitness])

		# Sort the population in descending order
		# -- "Maximize the objective function"
		self.population.sort(key=operator.itemgetter(1),reverse=True)


	def straight_line_distance(self, p1, p2):
		'''
			Return the Euclidian Distance between p1 and p2
		'''
		sld = math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)
		return sld


	def route_distance(self, individual):
		'''
			Determine the distance for the entire route
		'''
		distance = 0
		value = 0

		tour = individual + [self.origin_index]

		index = 0
		p1 = p2 = None
		while p2 != self.origin:
			p1 = self.points[tour[index]]
			p2 = self.points[tour[index+1]]
			distance += self.straight_line_distance(p1,p2)
			index += 1

		return distance


	def fitnessfcn(self, individual):
		'''
			Return the negative route distance so it can be maximized.
		'''
		return -self.route_distance(individual)


	"""
	select_parents()
	Purpose: 	This method selects 2 parents using the concept of Elitism. This method selects the top
				25% of a population and designates those as 'potential parents'. The parents are then
				randomly selected from this group. If a case appears where the parents are identical,
				then a completely random parent (for parent 2) is chosen from the population.
	"""
	def select_parents(self):
		'''
			Selects two parents from the population and returns them as a list
		'''
		potentialParents = self.population[0: int(len(self.population) * .25)] # filter population to only the top 25%

		# From the filtered list, randomly assign the parents.
		parent1 = self.population[random.randint(0, len(potentialParents))][0]
		parent2 = self.population[random.randint(0, len(potentialParents))][0]
		# If we happen to get the same parents, then just randomly choose a parent from the entire population.
		while parent1 == parent2:
			parent2 = self.population[random.randint(0, len(self.population) - 1)][0]

		return parent1, parent2


	"""
	reproduce(parent1, parent2)
	Parameters:		The two parents that will reproduce
	Purpose:		Generate two children from the passed in parents using the Ordered Crossover Strategy.
					This basically takes a random segment from one parent, gives that to the child, then
					fills the rest of the child's points (starting from the end of the inherited segment) using
					the *other* parent's genes until the child has the max number of genes.
	"""
	def reproduce(self,parent1,parent2):
		'''
			Reproduce using parent1 and parent2 and a crossover
			 strategy.
		'''



		''' Single point crossover:
			  Pull bits 0..crossover1 from parentX.
			  Pull remaining bits from parentY in the order they appear.
		'''
		# deep copy the parents for use in the following crossover.
		parent1_copy = copy.deepcopy(parent1)
		parent2_copy = copy.deepcopy(parent2)
		parent1_copy.remove(self.origin_index)
		parent2_copy.remove(self.origin_index)
		child1 = []
		child2 = []

		# Choose a random segment by assigning a beginning and end such that the end is always after the beginning.
		begin = random.randint(0, 47)
		end = (random.randint((begin + 1), 49))

		sublist = parent1_copy[begin:end]	# This will be the segment of Parent 1 that Child 1 will initially get.
		child1.extend(sublist)				# Give the segment to the child.
		sublist = parent2_copy[begin:end]	# This will be the segment of Parent 2 that Child 2 will initially get.
		child2.extend(sublist)				# Give the segment to the child.
		subset = []							# Create a subset for appending genes to the beginning of the child in the
											# right order.
		pointer = end						# The pointer will keep track of the index of the next gene additions. We
											# will start from the end position of the initial segment.
		while len(child1) < 49 - begin :	# so long as we still have the end to fill up. . .
			if pointer == 49:				# if we've gone past the array, go to the beginning
				pointer = 0
			if parent2_copy[pointer] not in child1 and pointer < 49:	# if the child doesn't have the gene and we haven't gone past the end...
				child1.append(parent2_copy[pointer])					# give the child this number.
				pointer = pointer + 1
			else:
				pointer = pointer + 1
			if pointer >48:
				pointer = 0												# if we've gone past, reset
		# do the same to fill up the beginning of the chromosome
		while len(subset) < 49 - len(child1):
			if pointer == 49:
				pointer = 0
			if parent2_copy[pointer] not in child1 and pointer < 49:
				subset.append(parent2_copy[pointer])
				pointer = pointer + 1
			else:
				pointer = pointer + 1
			if pointer > 48:
				pointer = 0

		child1 = subset + child1

		# perform the same operations on child 2, except fill the beginning and end with parent 1 genes.
		subset = []
		pointer = end

		while len(child2) < 49 - begin :
			if pointer == 49:
				pointer = 0
			if parent1_copy[pointer] not in child2 and pointer < 49:
				child2.append(parent1_copy[pointer])
				pointer = pointer + 1
			else:
				pointer = pointer + 1
			if pointer >48:
				pointer = 0

		while len(subset) < 49 - len(child2):
			if pointer == 49:
				pointer = 0
			if parent1_copy[pointer] not in child2 and pointer < 49:
				subset.append(parent1_copy[pointer])
				pointer = pointer + 1
			else:
				pointer = pointer + 1
			if pointer > 48:
				pointer = 0

		child2 = subset + child2

		# put the beginning point back onto the child chromosomes.
		child1 = [self.origin_index] + child1
		child2 = [self.origin_index] + child2

		return child1,child2

	"""
	mutate(child)
	Parameters: The child to mutate
	Purpose:	This method does 1 of 2 things: either it randomly swaps a gene with its immediate neighbor (10% of the time)
				or it performs a Reverse Sequence Mutation on the child (90% of the time).
	"""
	def mutate(self,child):
		'''
			Mutation Strategy
		'''

		# find random beginning and end points such that begin < end
		begin = random.randint(0, 45)
		end = random.randint(begin + 1, 49)

		child_copy = copy.deepcopy(child)
		child_copy.remove(self.origin_index)

		# 10% chance to randomly swap genes with their immediate neighbor.
		if(random.random() > 0.9):

			for x in range(0, 6):	# do the swap 6 times.
				randomPoint = random.randint(0,48)
				nextPoint = randomPoint + 1
				if(nextPoint >= self.chromosome_size - 1): #if the next gene is 'past the end', get the preceding neighbor.
					nextPoint = randomPoint - 1
				else:
					nextPoint = randomPoint + 1
				#swap the genes
				a, b = child_copy.index(child_copy[randomPoint]), child_copy.index(child_copy[nextPoint])
				child_copy[b], child_copy[a] = child_copy[a], child_copy[b]
		else:	# Otherwise, perform the Reverse Sequence Mutation (90% of the time)
			segment = child_copy[begin:end]
			child_copy[begin:end] = reversed(segment)

		child_copy = [self.origin_index] + child_copy	# prepend the origin back on the child.

		return child_copy




	def print_result(self):
		'''
			Displays the resulting route in the console.
		'''
		individual = self.population[0][0]
		fitness = self.population[0][1]

		print(" Final Route in %d Generations" % self.generations)
		print(" Final Distance : %5.3f\n" % -fitness)

		counter = 1

		for index in individual:
			print ("%2d. %s" % (counter, self.cities[index]))
			counter += 1

		print ("%2d. %s" % (counter, self.cities[self.origin_index]))


	def run(self):
		'''
			Run the genetic algorithm. Note that this method initializes the
			 first population.
		'''
		generations = 0

		self.initialize_population()

		last_fitness = 0
		fitness_counter = 0

		while generations <= self.generations:
			new_population = []
			parent1 = []
			parent2 = []

			while len(new_population) < self.population_size:

				parent1, parent2 = self.select_parents()
				child1, child2 = self.reproduce(parent1,parent2)

				# Generate a random number, and only mutate if the number
				#  is below the mutation rate.
				if (random.random() < self.mutation_rate):
					child1 = self.mutate(child1)
				if (random.random() < self.mutation_rate):
					child2 = self.mutate(child2)

				fitness1 = self.fitnessfcn(child1)
				fitness2 = self.fitnessfcn(child2)

				new_population.append([child1,fitness1])
				new_population.append([child2,fitness2])

			generations = generations + 1

			# Sort the new population in descending order
			new_population.sort(key=operator.itemgetter(1),reverse=True)

			self.population = new_population

			if generations % 1000 == 0 or generations >= self.generations:
				print("Generation: %d" % generations,"Fitness: %f" % self.population[0][1])
				if generations == self.generations:
					plotTSP(generations, self.population[0][0], self.points, self.population[0][1],True)
				else:
					plotTSP(generations, self.population[0][0], self.points, self.population[0][1],False)

			self.values.append(self.population[0][1])

		self.print_result()


if __name__ == '__main__':

	city_coordinates = "coordinates.txt"
	city_names = "cities.txt"
	start_city = "Johnson City, TN"
	locations = list(np.loadtxt(city_coordinates))
	cities = [line.rstrip('\n') for line in open(city_names)]
	points = []
	paths = []
	start_city_index = [i for i in range(len(cities)) if cities[i] == start_city][0]

	loc_x = [x for x,y in locations]
	loc_y = [y for x,y in locations]
	loc_c = ["black" for _ in range(len(locations))]

	for i in range(0, len(loc_x)):
		points.append((loc_x[i], loc_y[i]))

	#origin, generations, points, population_size, mutation_rate
	origin = (locations[start_city_index][0],locations[start_city_index][1])

	# Parameters: 1. origin location,
	#             2. number of generations,
	#             3. locations as a list of tuples,
	#             4. list of city names,
	#             5. number of individuals in each generation,
	#             6. mutation rate
	gs = GeneticSearch(origin, 10000, points, cities, 60, 0.40)
	gs.run()

	x = input("Press Enter to Exit...")
	plt.close()
